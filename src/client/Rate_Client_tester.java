package client;

import org.json.JSONException;

import com.google.gson.Gson;

import resources.Rates;

public class Rate_Client_tester {

	public static void main(String[] args) throws JSONException {
	
		
		Rate_Client rate_test  = new Rate_Client();
		Rates rates = new Rates();
		String src;
		String dest;
		Gson gson = new Gson();
		String jsonret = null;
		
		src = "A100";
		dest = "A200";
		
		rates = rate_test.get_rates(src, dest);
		
		jsonret = gson.toJson(rates);		
		System.out.println(jsonret);
		
		

	}

}
