package client;

import org.json.JSONException;

import com.google.gson.Gson;

import resources.ScheduleList;

public class Schedule_client_Tester {

	public static void main(String[] args) throws JSONException {
		
		Schedule_Client sclient = new Schedule_Client();
		ScheduleList slist = new ScheduleList();
		
		String src;
		String dest;
		Gson gson = new Gson();
		String jsonret = null;
		
		src = "A100";
		dest = "A200";
		
		slist = sclient.get_schedule(src, dest);
		jsonret = gson.toJson(slist);		
		System.out.println(jsonret);
		

	}

}
