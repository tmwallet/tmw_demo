package client;



import org.json.JSONException;
import com.google.gson.Gson;
import resources.Rts;

public class Route_client_tester {

	public static void main(String[] args) throws JSONException {
		
		
		Route_Client rtest = new Route_Client();
		Rts rr  = new Rts();
		String src;
		String dest;
		Gson gson = new Gson();
		String jsonret = null;
		
		
		
		src = "A100";
		dest = "A200";

		rr = rtest.get_routes(src, dest);
		
		jsonret = gson.toJson(rr);		
		System.out.println(jsonret);

	}

}
