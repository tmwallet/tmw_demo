package client;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import resources.Rate;
import resources.Rates;

public class Rate_Client {
	
	Client Rclient = Client.create();
	String Resource_URL = "https://routetesteri322078trial.hanatrial.ondemand.com/mockroute/webapi/myresource/getratesofroute";
	Gson gson = new Gson();
	String jsonret = null;
	String finaljsonret = null;
	int delimiter;
	
	public Rates get_rates(String src, String dest) throws JSONException
	{
		Rates Response_rates = new Rates();
		String Resource_URL_param = "?source=" + src + "&dest=" + dest + "";
		
		Resource_URL = Resource_URL + Resource_URL_param;

		WebResource webResource = Rclient.resource(Resource_URL);

		ClientResponse response = webResource.accept("text/plain").get(ClientResponse.class);

		if (response.getStatus() != 200) {

			Response_rates = null;
			}

		else {
		
			String Rate_output = response.getEntity(String.class);
			
			jsonret = gson.toJson(Rate_output);
			jsonret = jsonret.substring(0, 2) + '"' + jsonret.substring(2, jsonret.length());
			jsonret = jsonret.replace("\\", "");
			StringBuilder sb = new StringBuilder(jsonret);
			sb.deleteCharAt(0);
			sb.deleteCharAt(2);
			delimiter = sb.length();
			--delimiter;
			sb.deleteCharAt(delimiter);

			finaljsonret = sb.toString();
			
			JSONObject jObject = new JSONObject(finaljsonret);
			JSONArray rates = jObject.getJSONArray("rates");
			
			ArrayList<Rate> ratelist = new ArrayList<Rate>();
			int len = rates.length();
			
			for(int counter = 0; counter < len; ++counter)
			{
				
				Rate rate  = new Rate();
				JSONObject jsonrates = rates.getJSONObject(counter);
				
				rate.setCarrierName(jsonrates.getString("CarrierName"));
				rate.setPrice(jsonrates.getString("price"));
				rate.setCurrency(jsonrates.getString("currency"));
				rate.setOrigin(jsonrates.getString("origin"));
				rate.setOrigin_country_code(jsonrates.getString("origin_country_code"));
				rate.setDestination(jsonrates.getString("Destination"));
				rate.setDestination_country_code(jsonrates.getString("destination_country_code"));
				rate.setCargo_volume(jsonrates.getString("cargo_volume"));
				rate.setCommodity(jsonrates.getString("commodity"));
				rate.setMin_delivery_days(jsonrates.getString("min_delivery_days"));
				rate.setMax_delivery_days(jsonrates.getString("max_delivery_days"));
				rate.setServiceMethod(jsonrates.getString("serviceMethod"));
				
				ratelist.add(rate);
				
				
			}
			Response_rates.setRates(ratelist);
		}
		
		return Response_rates;
		
		
	}

}
