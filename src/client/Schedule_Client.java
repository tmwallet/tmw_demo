package client;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;


import resources.Schedule;
import resources.ScheduleList;

public class Schedule_Client {
	
	
	Client Sclient = Client.create();
	String Resource_URL = "https://routetesteri322078trial.hanatrial.ondemand.com/mockroute/webapi/myresource/getschedulebetween";
	Gson gson = new Gson();
	String jsonret = null;
	String finaljsonret = null;
	int delimiter;
	
	public ScheduleList get_schedule(String src, String dest) throws JSONException
	{
		ScheduleList Response_schedule = new ScheduleList();
		String Resource_URL_param = "?source=" + src + "&dest=" + dest + "";
		
		Resource_URL = Resource_URL + Resource_URL_param;

		WebResource webResource = Sclient.resource(Resource_URL);

		ClientResponse response = webResource.accept("text/plain").get(ClientResponse.class);

		if (response.getStatus() != 200) {

			Response_schedule = null;
			}

		else {
		
			String Schedule_output = response.getEntity(String.class);
			
			jsonret = gson.toJson(Schedule_output);
			jsonret = jsonret.substring(0, 2) + '"' + jsonret.substring(2, jsonret.length());
			jsonret = jsonret.replace("\\", "");
			StringBuilder sb = new StringBuilder(jsonret);
			sb.deleteCharAt(0);
			sb.deleteCharAt(2);
			delimiter = sb.length();
			--delimiter;
			sb.deleteCharAt(delimiter);

			finaljsonret = sb.toString();
			
			JSONObject jObject = new JSONObject(finaljsonret);
			JSONArray schedule = jObject.getJSONArray("schedule");
			
			ArrayList<Schedule> schedulelist = new ArrayList<Schedule>();
			int len = schedule.length();
			
			for(int counter = 0; counter < len; ++counter)
			{
				
				Schedule sch  = new Schedule();
				JSONObject jsonschedule = schedule.getJSONObject(counter);
				
				sch.setCarrierName(jsonschedule.getString("CarrierName"));
				sch.setOrigin(jsonschedule.getString("origin"));
				sch.setOrigin_country_code(jsonschedule.getString("origin_country_code"));
				sch.setDestination(jsonschedule.getString("Destination"));
				sch.setDestination_country_code(jsonschedule.getString("destination_country_code"));
				sch.setDuration(jsonschedule.getString("duration"));
				sch.setStartDate(jsonschedule.getString("startDate"));
				sch.setEndDate(jsonschedule.getString("endDate"));
				sch.setCarrier_type(jsonschedule.getString("Carrier_type"));
				
				schedulelist.add(sch);
				
				
			}
			Response_schedule.setSchedule(schedulelist);
		}
		
		return Response_schedule;
		
		
	}

}
