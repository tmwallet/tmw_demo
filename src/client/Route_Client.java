package client;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import resources.Rts;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import resources.Routes;
import resources.Step;

public class Route_Client {

	Client Rclient = Client.create();
	String Resource_URL = "https://routetesteri322078trial.hanatrial.ondemand.com/mockroute/webapi/myresource/getrouteparam";
	Gson gson = new Gson();
	String jsonret = null;
	String finaljsonret = null;
	int delimiter;

	public Rts get_routes(String src, String dest) throws JSONException {

		//Routes response_route = new Routes();
		String Resource_URL_param = "?src=" + src + "&dest=" + dest + "";
		Rts finalroutes = new Rts();
		Resource_URL = Resource_URL + Resource_URL_param;

		WebResource webResource = Rclient.resource(Resource_URL);

		ClientResponse response = webResource.accept("text/plain").get(ClientResponse.class);

		if (response.getStatus() != 200) {

			finalroutes = null;
			}

		else {

			
			String Route_output = response.getEntity(String.class);

			jsonret = gson.toJson(Route_output);
			jsonret = jsonret.substring(0, 2) + '"' + jsonret.substring(2, jsonret.length());
			jsonret = jsonret.replace("\\", "");
			StringBuilder sb = new StringBuilder(jsonret);
			sb.deleteCharAt(0);
			sb.deleteCharAt(2);
			delimiter = sb.length();
			--delimiter;
			sb.deleteCharAt(delimiter);

			finaljsonret = sb.toString();

			JSONObject jObject = new JSONObject(finaljsonret);
			JSONArray routes = jObject.getJSONArray("routes");
			 ArrayList<Routes> routelist = new ArrayList<Routes>();

			int len = routes.length();

			for (int counter = 0; counter < len; ++counter) {
				Routes route_obj = new Routes();
				JSONObject jsonroutes = routes.getJSONObject(counter);
				ArrayList<Step> steplist = new ArrayList<Step>();

				route_obj.setOrigin(jsonroutes.getString("Origin"));
				route_obj.setDestination(jsonroutes.getString("Destination"));
				
				JSONArray step = routes.getJSONObject(counter).getJSONArray("steps");

				
					for (int step_counter = 0; step_counter < step.length(); step_counter++) {

						JSONObject stepob = step.getJSONObject(step_counter);
						Step step_obj = new Step();
						
						step_obj.setStartAddress(stepob.getString("startAddress"));
						step_obj.setEndAddress(stepob.getString("EndAddress"));
						step_obj.setDistance(stepob.getString("distance"));
						step_obj.setTime(stepob.getString("time"));
						
						steplist.add(step_obj);
						
					}
					
					route_obj.setSteps(steplist);
					routelist.add(route_obj);
				}
				finalroutes.setRoutes(routelist);
			}
			
			return finalroutes;

	}
	

}
