package resources;

public class Rate {


private String CarrierName;
private String origin;
private String origin_country_code;
private String Destination;
private String destination_country_code;
private String price;
private String currency;
private String commodity;
private String serviceMethod;
private String cargo_volume;
private String min_delivery_days;
private String max_delivery_days;



public String getCarrierName() {
return CarrierName;
}


public void setCarrierName(String CarrierName) {
this.CarrierName = CarrierName;
}


public String getPrice() {
return price;
}


public void setPrice(String price) {
this.price = price;
}


public String getCurrency() {
return currency;
}


public void setCurrency(String currency) {
this.currency = currency;
}




public String getCommodity() {
	return commodity;
}

public void setCommodity(String commodity) {
	this.commodity = commodity;
}


public String getOrigin() {
	return origin;
}


public void setOrigin(String origin) {
	this.origin = origin;
}


public String getServiceMethod() {
	return serviceMethod;
}


public void setServiceMethod(String serviceMethod) {
	this.serviceMethod = serviceMethod;
}


public String getDestination() {
	return Destination;
}


public void setDestination(String destination) {
	Destination = destination;
}


public String getOrigin_country_code() {
	return origin_country_code;
}


public void setOrigin_country_code(String origin_country_code) {
	this.origin_country_code = origin_country_code;
}


public String getDestination_country_code() {
	return destination_country_code;
}


public void setDestination_country_code(String destination_country_code) {
	this.destination_country_code = destination_country_code;
}


public String getCargo_volume() {
	return cargo_volume;
}


public void setCargo_volume(String cargo_volume) {
	this.cargo_volume = cargo_volume;
}


public String getMin_delivery_days() {
	return min_delivery_days;
}


public void setMin_delivery_days(String min_delivery_days) {
	this.min_delivery_days = min_delivery_days;
}


public String getMax_delivery_days() {
	return max_delivery_days;
}


public void setMax_delivery_days(String max_delivery_days) {
	this.max_delivery_days = max_delivery_days;
}




}