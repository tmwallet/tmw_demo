package resources;

public class Schedule {

    private String CarrierName;
    private String Carrier_type;
    private String origin;
    private String Destination;
    private String origin_country_code;
    private String destination_country_code;
    private String duration;
    private String startDate;
    private String endDate;

    public String getCarrierName() {
        return CarrierName;
    }

    public void setCarrierName(String CarrierName) {
        this.CarrierName = CarrierName;
    }


    public String getOrigin() {
        return origin;
    }


    public void setOrigin(String origin) {
        this.origin = origin;
    }


    public String getStartDate() {
        return startDate;
    }


    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

	public String getDestination() {
		return Destination;
	}

	public void setDestination(String destination) {
		Destination = destination;
	}

	public String getCarrier_type() {
		return Carrier_type;
	}

	public void setCarrier_type(String carrier_type) {
		Carrier_type = carrier_type;
	}

	public String getOrigin_country_code() {
		return origin_country_code;
	}

	public void setOrigin_country_code(String origin_country_code) {
		this.origin_country_code = origin_country_code;
	}

	public String getDestination_country_code() {
		return destination_country_code;
	}

	public void setDestination_country_code(String destination_country_code) {
		this.destination_country_code = destination_country_code;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

}
