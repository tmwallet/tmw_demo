package resources;

import java.util.ArrayList;
import java.util.List;

public class Rts {

private List<Routes> routes = new ArrayList<Routes>();

/**
* 
* @return
* The routes
*/
public List<Routes> getRoutes() {
return routes;
}

/**
* 
* @param routes
* The routes
*/
public void setRoutes(List<Routes> routes) {
this.routes = routes;
}

}