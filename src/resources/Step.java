package resources;

public class Step {

private String startAddress;
private String EndAddress;
private String distance;
private String time;

/**
* 
* @return
* The startAddress
*/
public String getStartAddress() {
return startAddress;
}

/**
* 
* @param startAddress
* The start_address
*/
public void setStartAddress(String startAddress) {
this.startAddress = startAddress;
}

/**
* 
* @return
* The EndAddress
*/
public String getEndAddress() {
return EndAddress;
}

/**
* 
* @param EndAddress
* The End_address
*/
public void setEndAddress(String EndAddress) {
this.EndAddress = EndAddress;
}

/**
* 
* @return
* The distance
*/
public String getDistance() {
return distance;
}

/**
* 
* @param distance
* The distance
*/
public void setDistance(String distance) {
this.distance = distance;
}

/**
* 
* @return
* The time
*/
public String getTime() {
return time;
}

/**
* 
* @param time
* The time
*/
public void setTime(String time) {
this.time = time;
}

}