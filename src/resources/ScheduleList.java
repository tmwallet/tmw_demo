package resources;

import java.util.ArrayList;
import java.util.List;

public class ScheduleList {
	
	 private List<Schedule> schedule = new ArrayList<Schedule>();

	    /**
	     * 
	     * @return
	     *     The schedule
	     */
	    public List<Schedule> getSchedule() {
	        return schedule;
	    }

	    /**
	     * 
	     * @param schedule
	     *     The schedule
	     */
	    public void setSchedule(List<Schedule> schedule) {
	        this.schedule = schedule;
	    }

}
