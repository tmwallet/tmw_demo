package resources;

import java.util.ArrayList;
import java.util.List;



public class Routes {

private String Origin;
private String Destination;
private List<Step> steps = new ArrayList<Step>();

/**
* 
* @return
* The Origin
*/
public String getOrigin() {
return Origin;
}

/**
* 
* @param Origin
* The Origin
*/
public void setOrigin(String Origin) {
this.Origin = Origin;
}

/**
* 
* @return
* The Destination
*/
public String getDestination() {
return Destination;
}

/**
* 
* @param Destination
* The Destination
*/
public void setDestination(String Destination) {
this.Destination = Destination;
}

/**
* 
* @return
* The steps
*/
public List<Step> getSteps() {
return steps;
}

/**
* 
* @param steps
* The steps
*/
public void setSteps(List<Step> steps) {
this.steps = steps;
}

}