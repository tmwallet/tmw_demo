package resources;

import java.util.ArrayList;
import java.util.List;


public class Rates {

private List<Rate> rates = new ArrayList<Rate>();


/**
* 
* @return
* The rates
*/
public List<Rate> getRates() {
return rates;
}

/**
* 
* @param rates
* The rates
*/
public void setRates(List<Rate> rates) {
this.rates = rates;
}



}