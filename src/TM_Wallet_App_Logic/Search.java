package TM_Wallet_App_Logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.json.JSONException;



import client.Route_Client;
import resources.Routes;
import resources.Rts;
import resources.Step;
public class Search {

    private static final String START = "A100";
    private static final String END = "A200";
    Map<Integer, String[]> possible_routes = new HashMap<Integer, String[]>();
	List<String> routes_intermediate = new ArrayList<String>();

    public static void main(String[] args) throws JSONException {
        // this graph is directional
    	//calling routes
    	String source = "A100";
		String destination = "A200";
    	Route_Client route_client = new Route_Client();
    	//Routes routes_comb = new Routes();
    	Rts routes = new Rts();
    	routes = route_client.get_routes(source, destination);
    	
    	
    	
    	List<Routes> routes_array = new ArrayList<Routes>();
    	List<Step> steps_array = new ArrayList<Step>();
    	//routes_array.add(steps_array);
    	routes_array = routes.getRoutes();
    			
    	List<Step> steps = new ArrayList<Step>();
    	Step step_obj = new Step();
    	
    	Graph graph = new Graph();
    	
    	 
    	int routecount = 0;
    	while (routecount < routes_array.size()){
    		//steps_array = (List<Routes>) routes_array.get(routecount).getSteps();
    		steps_array = routes.getRoutes().get(routecount).getSteps();
    		int step_count = 0;
    		while (step_count < steps_array.size()){
    			String Start = steps_array.get(step_count).getStartAddress();
    			String End = steps_array.get(step_count).getEndAddress();
    			graph.addEdge(Start, End);
    			step_count++;
    		}
    		routecount++;
    	}
       
//        graph.addEdge("A", "B");
//        graph.addEdge("A", "C");
//        graph.addEdge("B", "A");
//        graph.addEdge("B", "D");
//        graph.addEdge("B", "E"); // this is the only one-way connection
//        graph.addEdge("B", "F");
//        graph.addEdge("C", "A");
//        graph.addEdge("C", "E");
//        graph.addEdge("C", "F");
//        graph.addEdge("D", "B"); main coding
//        graph.addEdge("E", "C");
//        graph.addEdge("E", "F");
//        graph.addEdge("F", "B");
//        graph.addEdge("F", "C");
//        graph.addEdge("F", "E");
        LinkedList<String> visited = new LinkedList();
        visited.add(START);
        new Search().breadthFirst(graph, visited);
    }

    private void breadthFirst(Graph graph, LinkedList<String> visited) {
        LinkedList<String> nodes = graph.adjacentNodes(visited.getLast());
        // examine adjacent nodes
        for (String node : nodes) {
            if (visited.contains(node)) {
                continue;
            }
            if (node.equals(END)) {
                visited.add(node);
                printPath(visited);
                visited.removeLast();
                break;
            }
        }
        // in breadth-first, recursion needs to come after visiting adjacent nodes
        for (String node : nodes) {
            if (visited.contains(node) || node.equals(END)) {
                continue;
            }
            visited.addLast(node);
            breadthFirst(graph, visited);
            visited.removeLast();
        }
    }

    private void printPath(LinkedList<String> visited) {
    	
    	 Integer  key;
    	 int i = 0 ;
    	 String[] route = null;
        for (String node : visited) {
        	
            System.out.print(node);
            System.out.print(" ");
            routes_intermediate.add(node);
        }
        i++;
        key = i;
        possible_routes.put(key, route);
        
        System.out.println();
        
        
        
    }
}